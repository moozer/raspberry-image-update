#!/usr/bin/env bash

# exit if anything fails.
set -e

if [ "x" == "x$1" ]; then
    echo "usage $0 <partition image>"
    exit 1
fi

MNTDIR="mnt"
PART_IMG=$1

echo "mounting $PART_IMG to $MNTDIR"
mkdir -p $MNTDIR

echo mounting
fuse-ext2 $PART_IMG $MNTDIR -o uid=0 -o rw+

echo making changes
cd $MNTDIR
../file-only/update_image.sh
cd ..
