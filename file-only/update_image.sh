#!/usr/bin/env bash

echo adding moozer ssh key to pi user
mkdir -p home/pi/.ssh
curl -s https://gitlab.com/moozer.keys > home/pi/.ssh/authorized_keys
