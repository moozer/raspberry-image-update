#!/usr/bin/env bash

# exit if anything fails.
set -e

if [ "x" == "x$1" ]; then
    echo "usage $0 <partition image>"
    exit 1
fi

MNTDIR="mnt"
PART_IMG=$1

echo "mounting $PART_IMG to $MNTDIR"
mkdir -p $MNTDIR
fuse-ext2 $PART_IMG $MNTDIR -o uid=0 -o rw+

echo making changes
TARGETPATH=home/pi/update_image.sh
cp -v full-update/update_image.sh $MNTDIR/$TARGETPATH

echo content of $MNTDIR
ls -a $MNTDIR

echo making changed
proot -0 -R $MNTDIR -w / -q qemu-arm-static /bin/bash /$TARGETPATH
rm $MNTDIR/$TARGETPATH
