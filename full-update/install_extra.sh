#!/usr/bin/env bash

# exit if anything fails.
set -e

apt-get install -y proot 
apt-get install -y qemu binfmt-support qemu-user-static
