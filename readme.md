Raspberry update
===================

This is a minimal project to show how to download, update and save a raspberry pi image. It must be run using sudo or as root.

Functionality
1. Download latest rasbian
2. extract
3. mount to mnt/
4. run update script
5. unmount
6. zip the result


This method is useful for changing files, it cannot be used for running any binaries from the raspberry - unless you you are running it on ARM hardware.

You will probably be able to run python/ruby/perl scripts using the binaries on the host machine.

The usual mounting scheme with `guestmount` and `mount` will not work inside a container for seurity reasons. [Fuseext2](https://github.com/alperakcan/fuse-ext2) is [suggested](https://forums.docker.com/t/mount-disk-image-from-inside-container/43591/2).

In this system we extract the /root partition to a separate file before using it, and then it gets merged back - Yes, we like to use diskspace.

