#!/usr/bin/env bash

# exit if anything fails.
set -e

if [ "x" == "x$1" ]; then
  echo "usage: $0 <new_image_name>"
  exit 1
fi
NEW_IMAGE_BASE=$1

NEW_IMAGE="$NEW_IMAGE_BASE.img"
NEW_IMAGE_ZIP="$NEW_IMAGE_BASE.zip"
MNTDIR="mnt"

echo unmounting
fusermount -u $MNTDIR

echo "merging partitions"
./merge_img.sh $NEW_IMAGE

echo zipping
zip $NEW_IMAGE_ZIP $NEW_IMAGE
