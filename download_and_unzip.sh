#!/usr/bin/env bash

# exit if anything fails.
set -e

DL_NAME="raspbian_lite_latest"
IMAGE_ZIP="raspberian_lite.zip"
IMAGE_URL="https://downloads.raspberrypi.org/$DL_NAME"

if [ "x" == "x$1" ]; then
  WORKDIR="./"
else
  WORKDIR=$1
fi

BINDIR=$(dirname $(readlink -f $0))
cd $WORKDIR

echo "Downloading from $IMAGE_URL to $IMAGE_ZIP (if updated)"
wget -nv -N $IMAGE_URL

echo hard linking
if [ -f $IMAGE_ZIP ]; then rm $IMAGE_ZIP; fi
ln $DL_NAME $IMAGE_ZIP

IMAGE=$(unzip -l $IMAGE_ZIP | grep .img | cut -d ' ' -f 7)
if [ -f $IMAGE ]; then rm $IMAGE; fi
echo "Using image file $IMAGE"
unzip $IMAGE_ZIP

IMAGE=$(unzip -l $IMAGE_ZIP | grep .img | cut -d ' ' -f 7)
echo splitting image file
$BINDIR/split_img.sh $IMAGE
